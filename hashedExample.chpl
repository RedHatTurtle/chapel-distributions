use HashedDist;

var D: domain(string) dmapped Hashed(idxType=string);
// Now D is a distributed associative domain (set) of strings
D += "Andre";
D += "Bruno";
D += "Carlos";
D += "Daniel";

var A: [D] int;
// Now A is a distributed associative array (map) from string to int
forall a in A do
  a = a.locale.id;

forall (key, value) in zip(D, A) {
  writeln(key, " -> ", value);
}
