module meshDistTest
{
    use Block1DDist;

    // Basic Test Parameters
    config const n = 6;

    // Mesh Test Parameters
    config const nDims    : int = 3;
    config const nVars    : int = 5;
    config const solOrder : int = 2;

    class mesh_c
    {
        const nDims : int;
        const nVars  : int;
        const solOrder : int;

        var nNodes : int;
        var nEdges : int;
        var nFaces : int;
        var nCells : int;
        var nBocos : int;
        var nFamls : int;

        // Array distributions
        //var nodeList_dist : dmap = new dmap(new Block(boundingBox={1..1}));
        //var nodeList_dist = new dmap(new Block(boundingBox={1..1}));
        //var edgeList_dist = new dmap(new Block(boundingBox={1..1}));
        //var faceList_dist = new dmap(new Block(boundingBox={1..1}));
        //var cellList_dist = new dmap(new Block(boundingBox={1..1}));
        //var bocoList_dist = new dmap(new Block(boundingBox={1..1}));
        var famlList_dist = new dmap(new Replicated());

        // Mesh Domains are all 1D therefore trivial do partition
        var nodeList_d : domain(rank=1, idxType=int);// dmapped nodeList_dist;
        var edgeList_d : domain(rank=1, idxType=int);// dmapped edgeList_dist;
        var faceList_d : domain(rank=1, idxType=int);// dmapped faceList_dist;
        var cellList_d : domain(rank=1, idxType=int);// dmapped cellList_dist;
        var bocoList_d : domain(rank=1, idxType=int);// dmapped bocoList_dist;
        var famlList_d : domain(rank=1, idxType=int);// dmapped Replicated();

        // FRMesh Domains
        var cellSPidx_d : domain(rank=2, idxType=int); // {1..nCells, 1..2}
        var faceFPidx_d : domain(rank=2, idxType=int); // {1..nFaces, 1..2}

        var  xyzSP_d : domain(rank=2, idxType=int);    // {1..nSPs, 1..nDims}
        var  xyzFP_d : domain(rank=2, idxType=int);    // {1..nFPs, 1..nDims}

        var  metSP_d : domain(rank=3, idxType=int);    // {1..nSPs, 1..nDims, 1..nDims}       nDims+1 if moving mesh
        var  jacSP_d : domain(rank=1, idxType=int);    // {1..nSPs}                           nDims+1 if moving mesh

        var  solSP_d : domain(rank=2, idxType=int);    // {1..nVars, 1..nSPs}
        var  solFP_d : domain(rank=3, idxType=int);    // {1..nFPs, 1..2, 1..nVars}
        var  flxFP_d : domain(rank=3, idxType=int);    // {1..nFPs, 1..2, 1..nVars}

        proc init()
        {
            
        }
    }

    proc main() {

        var testMesh = new unmanaged mesh_c(nDims=3, nVars=5, solOrder=2);
        writeln(testMesh);
    }
}
