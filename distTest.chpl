module distTest
{
    use BlockDist;
    use Block1DDist;
    use CyclicDist;
    use BlockCycDist;
    use StencilDist;
    use ReplicatedDist;
    use PrivateDist;

    // Basic Test Parameters
    config const n = 6;

    proc main() {


        // Private Dist Test
        {
            writeln("Private Array Tests:");
            writeln();

            var privDist = new dmap(new Private());
            var privDom  : domain(rank=1, idxType=int) dmapped Private();
            var privArr  : [privDom] int = -1;

            writeln("Private Array's domain: ", privArr.domain);

            writeln();
            writeln("Private array can be initialized at declaration");
            for loc in Locales do
                on loc do
                    writeln("Locale ", loc.id, " - ReplArr: ", privArr);

            writeln();
            forall elem in privArr do
                elem = -1;
            forall elem in privArr do
                elem = here.id;
            writeln("Private mapped arrays are globally visible");
            for loc in Locales do
                on loc do
                    writeln("Locale ", loc.id, " - PrivArr: ", privArr);
            writeln("So a `forall` loop over it is by default distributed");

            writeln();
            writeln("And using an `on` clause causes a remote assignment");
            forall elem in privArr do
                elem = -1;
            forall loc in Locales do
                on loc do
                    privArr = here.id;
            for loc in Locales do
                on loc do
                    writeln("Locale ", loc.id, " - PrivArr: ", privArr);
            writeln("The array view is the same from any locale");
        }

        writeln();
        writeln("----------------------------------------");
        writeln();

        // Replicated Dist Test
        {
            writeln("replicated array tests:");
            writeln();

            // Distributions can't be default initialized so they need to be assigned at declaration
            var replDist = new dmap(new Replicated());
            var replDom  : domain(rank=1, idxType=int) dmapped replDist;
            var replArr  : [replDom] int = -1;
            replDom = {1..numLocales};

            writeln("Replicated Array's domain: ", replArr.domain);

            writeln();
            writeln("Replicated array can't be initialized at declaration");
            for loc in Locales do
                on loc do
                    writeln("Locale ", loc.id, " - ReplArr: ", replArr);

            writeln();
            on Locales[1] do
                forall elem in replArr do elem = -1;
            writeln("Replicated mapped arrays are only locally visible");
            for loc in Locales do
                on loc do
                    writeln("Locale ", loc.id, " - ReplArr: ", replArr);
            writeln("So a forall loop over it only alters the current locale's replica");

            writeln();
            writeln("To access a replica in another locale you must use an `on` clause which starts a local taks on that locale");
            forall loc in Locales do
                on loc do
                    replArr = here.id;
            for loc in Locales do
                on loc do
                    writeln("Locale ", loc.id, " - ReplArr: ", replArr);
            writeln("Each replica is private to it's locale");
            writeln("To access a remote replica you need to go to that locale and copy the values to a temporary communication variable");
        }

        writeln();
        writeln("----------------------------------------");
        writeln();

        // Hashed Dist Tests
        {
            writeln("Hashed Array Tests:");
            writeln();
        }

        writeln();
        writeln("----------------------------------------");
        writeln();

        // Block / Block1D Dists
        {
            var domain1d     : domain(rank=1, idxType=int);
            var domain2d     : domain(rank=2, idxType=int);
            var domain3d     : domain(rank=3, idxType=int);

            domain1d = {1..n};
            domain2d = {1..n, 1..5};
            domain3d = {1..n, 1..3, 1..5};

            var domain1d_block = domain1d dmapped Block(  boundingBox=domain1d);
            var domain1d_blk1d = domain1d dmapped Block1D(boundingBox=domain1d);
            var domain2d_block = domain2d dmapped Block(  boundingBox=domain2d);
            var domain2d_blk1d = domain2d dmapped Block1D(boundingBox=domain2d);
            var domain3d_block = domain3d dmapped Block(  boundingBox=domain3d);
            var domain3d_blk1d = domain3d dmapped Block1D(boundingBox=domain3d);
            var array1d_block : [domain1d_block] int;
            var array1d_blk1d : [domain1d_blk1d] int;
            var array2d_block : [domain2d_block] int;
            var array2d_blk1d : [domain2d_blk1d] int;
            var array3d_block : [domain3d_block] int;
            var array3d_blk1d : [domain3d_blk1d] int;

            writeln("Default Locale domain:");
            writeln(Locales.domain);

            // Get the IDs of the locales each element was distributed to and print
            forall elem in array1d_block do elem = here.id;
            forall elem in array1d_blk1d do elem = here.id;
            forall elem in array2d_block do elem = here.id;
            forall elem in array2d_blk1d do elem = here.id;
            forall elem in array3d_block do elem = here.id;
            forall elem in array3d_blk1d do elem = here.id;

            writeln();
            writeln("Partition Maps for 1d Array");
            writeln( array1d_block );
            writeln( array1d_blk1d );

            writeln();
            writeln("Partition Map for 2d Array Block Distributed");
            writeln("Block dist | Block1D dist");
            for row in domain2d.dim(0) do
                writeln( array2d_block[row, ..], "   |   ", array2d_blk1d[row, ..] );

            writeln();
            writeln("Initial Partition Map for 3d Array");
            writeln("Block dist | Block1D dist");
            for i in domain3d.dim(0)
            {
                writeln();
                writeln("Level ", i, ":");
                for j in domain3d.dim(1) do
                    writeln(array3d_block[i, j, ..], "   |   ", array3d_blk1d[i, j, ..]);
            }
        }

        //writeln();
        //writeln("----------------------------------------");
        //writeln();

        // Block vs Stencil
        {
            //writeln("Stencil Array Tests:");
            //writeln();
        }

        //writeln();
        //writeln("----------------------------------------");
        //writeln();

        // Dimensional 2D Dists
        {
            //writeln("Dimensional Array Tests:");
            //writeln();
        }
    }
}
