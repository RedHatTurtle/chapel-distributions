use PrivateDist;

const PrivateSpace: domain(1) dmapped Private();

var A: [PrivateSpace] int;
forall a in A do
    a = here.numPUs();

forall idx in A.domain do
    writeln(idx, " -> ", A[idx]);

for idx in 0..3 do
    writeln(A[idx]);
