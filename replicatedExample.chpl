use ReplicatedDist;

const Dbase = {1..5};  // A default-distributed domain
const Drepl = Dbase dmapped Replicated();
var Abase: [Dbase] int;
var Arepl: [Drepl] int;

// Only the current locale's replicand is accessed
Arepl[3] = 4;

// Only the current locale's replicand is accessed
forall (b,r) in zip(Abase,Arepl) do b = r;
Abase = Arepl;

// Access other locales' replicand with the replicand(loc) method
Arepl.replicand(Locales[1]) = Arepl;

writeln(Arepl);
